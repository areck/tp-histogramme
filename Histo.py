import cv2
import sys
import numpy as np
import re
import math
from matplotlib import pyplot as plt

pattern = "^.*\.(jpg)$"
flagVideo = False
x1= 120
y1= 40
x2 = 400
y2 = 340

width = 640
height = 480

isPaused = False

print(cv2.__version__)

if len(sys.argv) > 1:
    args = str(sys.argv[1])
    if re.match(pattern, args) is not None:
        src = str(sys.argv[1])
        print(str(sys.argv[1]))
        img = cv2.imread(src, cv2.IMREAD_UNCHANGED)
    else:
        src = cv2.VideoCapture(str(sys.argv[1]))
        ret, img = src.read()
        flagVideo=True
else:
    src = cv2.VideoCapture(0)
    ret, img = src.read()

#print("Camera opened : " + str(cap.isOpened()))



fig, ax = plt.subplots()
ax.set_title('Histogram')
ax.set_xlabel('Bin')
ax.set_ylabel('Frequency')

lw = 3
alpha = 0.5
bins = 256

lineGray, = ax.plot(np.arange(bins), np.zeros((bins,1)), c='k', lw=lw)
ax.set_xlim(0, bins)
ax.set_ylim(0, 3000)
plt.ion()
plt.show()

#gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ROI = img[y1*2:y2, x1*2:x2]
hist = cv2.calcHist([ROI],[0],None,[256],[0,256])



while (True):
    if (len(sys.argv) == 1 or flagVideo) and not isPaused:
        ret, img = src.read()
        #gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ROI = img[y1*4:y2, x1*4:x2*2]
        hist = cv2.calcHist([ROI], [0], None, [256], [0, 256])
        print(hist)
    ROIGRAY = cv2.cvtColor(ROI, cv2.COLOR_BGR2GRAY)
    ret, seuil = cv2.threshold(ROIGRAY, 60, 255, cv2.THRESH_TOZERO)
    element = cv2.getStructuringElement(cv2.MORPH_RECT, (25, 25))
    seuil = cv2.dilate(seuil, element)
    seuil = cv2.erode(seuil, element)
    contours, hierarchy = cv2.findContours(seuil, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    lineGray.set_ydata(hist)
    cv2.imshow('ROI',ROI)
    cv2.imshow('Seuillage', seuil)
    imageContour = cv2.drawContours(ROIGRAY, contours, -1, (255,255,180), 3)
    cv2.imshow('Contours', imageContour)
    fig.canvas.draw()
   # cv2.imshow('Sobel de premier ordre en X', derivX)
    #cv2.imshow('Sobel de premier ordre en Y', derivY)
    #cv2.imshow('Sobel X + Y', G)



    key = cv2.waitKey(1)
    if key == 27:
        break
    if key == 32:
       if isPaused:
           isPaused = False
       else:
           isPaused = True









src.release()
cv2.destroyAllWindows()
